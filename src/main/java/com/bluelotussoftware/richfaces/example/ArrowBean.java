/*
 * Copyright 2011-2013 John Yeary <jyeary@bluelotussoftware.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * $Id$
 */
package com.bluelotussoftware.richfaces.example;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@ManagedBean
//@SessionScoped
@ViewScoped
public class ArrowBean implements Serializable {

    private static final long serialVersionUID = 1251673903844432802L;
    private List<Arrow> source;
    private List<Arrow> serialTarget;
    private List<Arrow> parallelTarget;
    private List<Arrow> target;

    public ArrowBean() {
        source = new ArrayList<Arrow>();
        serialTarget = new ArrayList<Arrow>();
        parallelTarget = new ArrayList<Arrow>();
        target = new ArrayList<Arrow>();
        reset();
    }

    public List<Arrow> getSource() {
        return source;
    }

    public List<Arrow> getTarget() {
        return target;
    }

    public void move(Arrow arrow) {
        source.remove(arrow);
        target.add(arrow);
    }

    public List<Arrow> getParallelTarget() {
        return parallelTarget;
    }

    public void setParallelTarget(List<Arrow> parallelTarget) {
        this.parallelTarget = parallelTarget;
    }

    public List<Arrow> getSerialTarget() {
        return serialTarget;
    }

    public void setSerialTarget(List<Arrow> serialTarget) {
        this.serialTarget = serialTarget;
    }

    public void add(Arrow arrow) throws CloneNotSupportedException {
        serialTarget.add(arrow.clone());
    }

    public final void reset() {
        source.clear();
        target.clear();
        serialTarget.clear();
        parallelTarget.clear();
        for (long n = 0; n < 10L; n++) {
            source.add(new Arrow(n, "Arrow " + n, null));
        }
    }
}
