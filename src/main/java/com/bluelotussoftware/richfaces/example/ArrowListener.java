/*
 * Copyright 2011-2013 John Yeary <jyeary@bluelotussoftware.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.richfaces.example;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.richfaces.event.DropEvent;
import org.richfaces.event.DropListener;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@ManagedBean
@RequestScoped
public class ArrowListener implements DropListener {

    @ManagedProperty(value = "#{arrowBean}")
    private ArrowBean arrowBean;

    public void setArrowBean(ArrowBean arrowBean) {
        this.arrowBean = arrowBean;
    }

    @Override
    public void processDrop(DropEvent event) {
        Arrow filter = (Arrow) event.getDragValue();
        String id = event.getDropTarget().getClientId(FacesContext.getCurrentInstance());
        if ("form1:dropTarget2".equals(id)) {
            try {
                arrowBean.add(filter);
            } catch (CloneNotSupportedException ex) {
                Logger.getLogger(ArrowListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            arrowBean.move(filter);
        }
    }
}